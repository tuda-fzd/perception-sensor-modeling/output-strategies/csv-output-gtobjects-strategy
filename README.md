# Ground Truth Objects to .csv Output Strategy

This strategy writes ground truth objects in a csv file.
Path for the file is set via CMakeLists.

NOTE:
This strategy needs transformation-functions from ../transformation-functions.
Include the [Transformation Functions](https://gitlab.com/tuda-fzd/perception-sensor-modeling/transformation-functions) as a submodule within the Framework. 